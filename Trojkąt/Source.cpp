﻿/*Program przeksztalca podane boki trójkąta na podstawie podanego przez uzytkownika współczynika skali, a następnie wyświetla dane
Łukasz Szuca, s10927
*/

#include <iostream>
#include <math.h> //needed for sqrt function
using namespace std;

class Triangle {																				//private Triangle class variables
	double m_sideA;
	double m_sideB;
	double m_sideC;
public:
	

	Triangle(double a, double b, double c) {													//setter for triangle object variables
		m_sideA = a;
		m_sideB = b;
		m_sideC = c;
	}
	void scale(double scale) {																	//changes value of triangle object variable
		
		m_sideA = m_sideA * scale;
		m_sideB = m_sideB * scale;
		m_sideC = m_sideC * scale;
		
	}
	void showSides() {																			//shows actual value of triangle object variable
		cout << m_sideA << ", " << m_sideB << ", " << m_sideC << "\n";
	}
	
	friend double area(Triangle triangle);														//friend function declaration
};


double area(Triangle triangle) {
	double p = 0.5 * (triangle.m_sideA + triangle.m_sideB + triangle.m_sideC);					// half of triangle circuit
	double s = sqrt(p*(p - triangle.m_sideA)*(p - triangle.m_sideB)*(p - triangle.m_sideC));	// triangle area
	return s;
}

int main() {

	Triangle triangle(4, 15, 18);
	cout << "Triangle sides before scaling: ";
	triangle.showSides();
	cout << "\nArea before scaling: " << area(triangle);
	cout << "\n";

	double scale;
	cout << "Scale parameter: ";
	cin >> scale;
	triangle.scale(scale);
	
	cout << "\nTriangle sides after scaling: ";
	triangle.showSides();
	cout << "\nArea after scaling: " << area(triangle);
	cout << "\n";
	
	return 0;

}